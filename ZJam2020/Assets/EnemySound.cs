﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySound : MonoBehaviour
{
    private AudioSource source;
    [SerializeField] AudioClip whistle;
    void Start()
    {
        source = GetComponent<AudioSource>();
        Invoke("PlayWhistle", Random.Range(1, 3));
    }

    private void PlayWhistle()
    {
        source.PlayOneShot(whistle);
        Invoke("PlayWhistle", Random.Range(3, 6) + whistle.length);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}