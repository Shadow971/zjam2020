﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyIA : MonoBehaviour
{
    public Transform wayCarpet;
    public Material spritePointForward;
    public Material spritePointBack;
    MeshRenderer sprite;
    GameObject player;
    bool warning = false;
    public int distance = 10 ;
    NavMeshAgent navMeshAgent;
    AlignToCamera spriteController;
    Transform[] waypoints;
    float alert = 0;
    int index = 0;
    public int time = 1;

    private List<Vector3> footPrintPositions;
    public Texture footPrintTexture;

    private void Start()
    {
        sprite = transform.GetChild(0).GetComponent<MeshRenderer>();
        player = GameObject.Find("Player");

        waypoints = new Transform[wayCarpet.childCount];
        
        for (int i = 0; i < wayCarpet.childCount; i++ )
            waypoints[i] = wayCarpet.GetChild(i);

        navMeshAgent = GetComponent<NavMeshAgent>();
        waypoints[index].gameObject.GetComponent<BoxCollider>().enabled = true;
        navMeshAgent.SetDestination(waypoints[index].position);

        spriteController = GetComponentInChildren<AlignToCamera>();

        //
        footPrintPositions = new List<Vector3>(6);
        StartCoroutine(FootPrintCycle(0.5f));
    }

    private void Update()
    {
        if (transform.forward.z > 0)
        {
            sprite.material = spritePointForward;
            //Debug.Log("Putting front material, " + sprite.material);
        }
        if (transform.forward.z < 0)
        {
            sprite.material = spritePointBack;
            //Debug.Log("Putting back material, " + sprite.material);
        }
        float difference = Vector3.Distance(transform.position, player.transform.position);
        Vector3 playerDirection = player.transform.position - transform.position;
        //Debug.DrawRay(transform.position, playerDirection);

        if(warning)
        {
            navMeshAgent.SetDestination(player.transform.position);
        }
        if (difference < distance)
        {
            float angle = Vector3.Angle(playerDirection, transform.forward);
            if (angle < 15 && angle > -15)
            {
                if (Physics.Raycast(transform.position, playerDirection, out RaycastHit hit, playerDirection.magnitude))
                {
                    //Debug.Log(hit.collider.gameObject.name);
                    if (hit.collider.gameObject.CompareTag("Player"))
                    {
                        warning = true;
                    }
                }
            }
        }
        
        if(warning && difference < 1f)
        {
                player.GetComponent<PlayerController>().GetCaught();
        }

        if (!player.activeSelf)
        {
            ChangeDestination();
            warning = false;
        }

        if (warning && difference > distance)
        {
            warning = false;
            ChangeDestination();
        }

        // Si recibe un 0,0 para
        if (spriteController != null)
            spriteController.Bounce(Vector2.one);
        else
            spriteController = GetComponentInChildren<AlignToCamera>();
    }

    //private void OnDrawGizmos()
    //{
    //    for (int i = 0; i < footPrintPositions.Count; i++)
    //    {
    //        Gizmos.DrawSphere(footPrintPositions[i], 1);
    //    }
    //}

    private void OnGUI()
    {
        for(int i = 0; i < footPrintPositions.Count; i++)
        {
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(footPrintPositions[i]);
            GUI.DrawTexture(new Rect(screenPosition.x - 16, Screen.height - screenPosition.y + 16, 32, 32), footPrintTexture);
        }
    }

    void ChangeDestination()
    {
        waypoints[index].gameObject.GetComponent<BoxCollider>().enabled = false;

        index++;
        if (index >= waypoints.Length)
        {
            index = 0;
        }
        navMeshAgent.SetDestination(waypoints[index].position);
        waypoints[index].gameObject.GetComponent<BoxCollider>().enabled = true;
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Waypoint"))
        {
            ChangeDestination();
        }
        if (other.CompareTag("Princess"))
        {
            navMeshAgent.SetDestination(other.transform.position);
        }
    }

    private IEnumerator FootPrintCycle(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            footPrintPositions.Add(transform.position + Vector3.down);
            if (footPrintPositions.Count > 5) footPrintPositions.RemoveAt(0);
            //Debug.Log("Adding footpirnt");
        }
    }
}
