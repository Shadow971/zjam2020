﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreamController : MonoBehaviour
{
    public GameObject blood;

    private AlignToCamera spriteController;
    private bool screaming = false;
    private EndLevel[] endLevels;

    // Start is called before the first frame update
    void Start()
    {
        spriteController = GetComponentInChildren<AlignToCamera>();
        endLevels = FindObjectsOfType<EndLevel>();
    }

    // Update is called once per frame
    void Update()
    {
        if (screaming)
        {
            if (spriteController != null)
                spriteController.Bounce(Vector2.one);
            else
                spriteController = GetComponentInChildren<AlignToCamera>();
        }
            
    }


    public void ScreamAndBleed()
    {
        blood.SetActive(true);
        screaming = true;
    }

    public void JustDie()
    {
        blood.SetActive(false);
    }


}