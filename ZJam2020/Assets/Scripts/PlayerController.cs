﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Light playerLight;

    private float maxHealth = 100;
    [SerializeField] float healthMult = 10f;
    [SerializeField] float addHealthMult = 2f;
    private float health = 20;

    private bool canMove = true;
    private bool eating = false;
    private bool princessNear = false;
    GameObject princess;
    private bool endLevelNear = false;
    GameObject endLevel;

    Vector3 offset = Vector3.zero;
    [SerializeField] float maxSpeed = 3;
    [SerializeField] float accelRate = 6;

    private CharacterController characterController;
    private AlignToCamera spriteController;

    private AudioSource source;
    private AudioSource princessSource;
    [SerializeField] AudioClip scream;
    [SerializeField] AudioClip steps;

    [SerializeField] Slider slider;

    private float lightRange;

    private bool onCloset = false;

    private Vector2 moveInputs;

    public Vector2 MoveInputs { get { return moveInputs; } }

    public GUISkin uISkin;
    public Texture victoryTexture;
    public Texture bloodDefeatTexture;
    public Texture guardDefeatTexture;

    private bool victory = false;
    private bool bloodDefeat = false;
    private bool guardDefeat = false;
    private bool textActive = false;
    private bool flashed = false;

    private string message;

    //
    private MeshRenderer spriteRenderer;
    public Material frontMaterial;
    public Material backMaterial;

    List<string> text;

    public Text actionText;

    void Start()
    {
        StartCoroutine(InitializeComponents());
    }

    void Update()
    {
        float dt = Time.deltaTime;
        
        if (eating)
        {
            AddHealth();
        }
        else
        {
            if(onCloset)
            {
                health -= dt * healthMult / 2;
            }
            else
            {
                health -= dt * healthMult;
            }
        }
        slider.value = health / maxHealth;
        
        //playerLight.range = health / lightRange;

        if (canMove)
        {
            Movement(dt);
        }

        if (princessNear)
        {
            Attack();
        }

        //Ñapaaaa
        transform.position = new Vector3(transform.position.x, 1, transform.position.z);

        //if (endLevelNear)
        //{
        //    End();
        //}

        if (health <= 0)
        {
            //Destroy(gameObject);
            bloodDefeat = true;
            Time.timeScale = 0;
        }
        else if (health >= 100)
        {
            victory = true;
            Time.timeScale = 0;
        }

        // Chequearemos aqui victoria y derrota
        if (victory)
        {
            if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown("joystick button 2"))
            { 
                // TODO: Aqui cambiaremos a la escena final
                Time.timeScale = 1;
                SceneManager.LoadScene("End Scene");
            }
        }
        if(bloodDefeat || guardDefeat)
        {
            if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown("joystick button 2"))
            {
                Time.timeScale = 1;
                Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
            }
            
        }

        //
        if (Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale = 0;
            victory = true;
        }
    }

    IEnumerator InitializeComponents()
    {
        //
        yield return new WaitForSeconds(0.1f);
        //
        text = new List<string>();
        TextAsset messagesFile = Resources.Load<TextAsset>("Messages");
        //string path = Application.dataPath + "/Messages.txt";
        string[] messages;
        //messages = File.ReadAllLines(path);
        Debug.Log(messagesFile.text);
        messages = messagesFile.text.Split('\n');

        foreach (string m in messages)
        {
            text.Add(m);
        }
        source = GetComponent<AudioSource>();
        playerLight = transform.GetChild(0).GetComponent<Light>();
        lightRange = playerLight.range;
        characterController = this.gameObject.GetComponent<CharacterController>();
        spriteController = GetComponentInChildren<AlignToCamera>();
        spriteRenderer = GetComponentInChildren<MeshRenderer>();
        //
        actionText.text = "";
        //
        Time.timeScale = 1;
        characterController.enabled = false;
        offset = characterController.transform.position;
        characterController.enabled = true;
    }

    IEnumerator Flash()
    {
        flashed = true;
        yield return new WaitForSeconds(Time.deltaTime);
        textActive = !textActive;
        flashed = false;
    }

    private void OnGUI()
    {
        //
        //uISkin.customStyles[0].normal.textColor = Color.red
        if (eating)
        {
            if(!flashed)
                StartCoroutine(Flash());
                
            if(textActive)
                GUI.Label(new Rect(0, 0, Screen.width, Screen.height), message, uISkin.customStyles[2]);
        }
        //
        if (victory)
        {
            GUI.DrawTexture(new Rect(Screen.width * 1/4, 0, Screen.width * 1/2, Screen.height), victoryTexture);
            GUI.Label(new Rect(Screen.width * 1 / 4, Screen.height * 3/4, Screen.width * 1 / 2, 30), 
                "Another night of beauty, thought the countess", uISkin.customStyles[0]);
            GUI.Label(new Rect(Screen.width * 1 / 4, Screen.height * 3 / 4 + 30, Screen.width * 1 / 2, 30), "PRESS ACTION TO END", uISkin.customStyles[1]);
        }
        if (bloodDefeat)
        {
            GUI.DrawTexture(new Rect(Screen.width * 1 / 4, 0, Screen.width * 1 / 2, Screen.height), bloodDefeatTexture);
            GUI.Label(new Rect(Screen.width * 1 / 4, Screen.height * 3 / 4, Screen.width * 1 / 2, 30), 
                "The blood lust consumed the countess, as such as her sanity", uISkin.customStyles[0]);
            GUI.Label(new Rect(Screen.width * 1 / 4, Screen.height * 3 / 4 + 30, Screen.width * 1 / 2, 30), "PRESS ACTION TO REPEAT", uISkin.customStyles[1]);
        }
        if (guardDefeat)
        {
            GUI.DrawTexture(new Rect(Screen.width * 1 / 4, 0, Screen.width * 1 / 2, Screen.height), guardDefeatTexture);
            GUI.Label(new Rect(Screen.width * 1 / 4, Screen.height * 3 / 4, Screen.width * 1 / 2, 30), 
                "The countess was caught, and executed by her crimes", uISkin.customStyles[0]);
            GUI.Label(new Rect(Screen.width * 1 / 4, Screen.height * 3 / 4 + 30, Screen.width * 1 / 2, 30), "PRESS ACTION TO REPEAT", uISkin.customStyles[1]);
        }
    }

    private void FixedUpdate()
    {
        actionText.text = "";
    }

    public void OnCloset(bool b) { onCloset = b; }

    //private void End()
    //{
    //    if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown("joystick button 2"))
    //    {
    //        //Fade
    //        Transform newTransform = endLevel.GetComponent<EndLevel>().Goto();
    //        transform.position = newTransform.position;
    //        transform.rotation = newTransform.rotation;
    //        //
    //        Transform nextCameraPosTransform = endLevel.GetComponent<EndLevel>().GotoCamera();
    //        Camera.main.transform.parent.position = nextCameraPosTransform.position;
    //    }
    //}

    private void Attack()
    {
        if ((Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown("joystick button 2")) && !eating)
        {
            eating = true;
            canMove = false;
            //
            if (text == null)
            {
                text = new List<string>();
                string path = Application.dataPath + "/Messages.txt";
                string[] messages;
                messages = File.ReadAllLines(path);
            }
            //
            SphereCollider co = princess.GetComponent<SphereCollider>();
            co.enabled = true;
            StartCoroutine(Eat(princess));
            int rand = Random.Range(0, text.Count - 1);
            message = text[rand];  
            text.RemoveAt(rand);
            //
            ScreamController screamController = princess.GetComponent<ScreamController>();
            screamController.ScreamAndBleed();
            princessSource = screamController.GetComponent<AudioSource>();
            princessSource.PlayOneShot(scream);
        }
    }

    private void AddHealth()
    {
        if (health < maxHealth)
        {
            health += Time.deltaTime * addHealthMult * healthMult;
            health = Mathf.Clamp(health, 0, maxHealth);
        }
    }

    private void Movement(float dt)
    {
        //Vector2 moveInputs;
        moveInputs.x = Input.GetAxisRaw("Horizontal");
        moveInputs.y = Input.GetAxisRaw("Vertical");

        offset.z += GetOffset(dt, moveInputs.y, maxSpeed, offset.z, accelRate);
        offset.x += GetOffset(dt, moveInputs.x, maxSpeed, offset.x, accelRate);

        if (spriteController != null)
            spriteController.Bounce(moveInputs);
        else
            spriteController = GetComponentInChildren<AlignToCamera>();

        if (MoveInputs.x != 0 || MoveInputs.y != 0)
        {
            if( source == null )
            {
                source = GetComponent<AudioSource>();
            }

            if (source != null && !source.isPlaying)
                source.PlayOneShot(steps);
            //
            if (moveInputs.y < 0)
            {
                if(spriteRenderer != null)
                {
                    spriteRenderer.material = frontMaterial;
                    //Debug.Log("Putting front material, " + spriteRenderer.material);
                }
                else
                {
                    spriteRenderer = GetComponentInChildren<MeshRenderer>();
                }

            }
            else if (moveInputs.y > 0)
            {
                if (spriteRenderer != null)
                {
                    spriteRenderer.material = backMaterial;
                    //Debug.Log("Putting back material, " + spriteRenderer.material);
                }
                else
                {
                    spriteRenderer = GetComponentInChildren<MeshRenderer>();
                }
            }
        }

        if(characterController != null)
        {
            characterController.Move(offset * dt);
        }
        else
        {
            characterController = this.gameObject.GetComponent<CharacterController>();
        }
           

        
    }

    public float GetOffset(float dt, float input, float max, float currentSpeed, float accel)
    {
        float targetZSpeed = input * max;
        float velZOffset = targetZSpeed - currentSpeed;
        velZOffset = Mathf.Clamp(velZOffset, -accel * dt, accel * dt);
        return velZOffset;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Princess"))
        {
            princess = other.gameObject;
            princessNear = true;
        }
        //if(other.CompareTag("EndLevel"))
        //{
        //    endLevel = other.gameObject;
        //    endLevelNear = true;
        //}
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Princess"))
        {
            actionText.text = "Rejuvenate";
        }
        else if (other.CompareTag("HidePoint"))
        {
            actionText.text = "Hide";
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Princess"))
        {
            princessNear = false;
        }
        //if (other.CompareTag("EndLevel"))
        //{
        //    endLevelNear = false;
        //}
    }

    public void GetCaught()
    {
        Time.timeScale = 0;
        guardDefeat = true;
    }

    IEnumerator Eat(GameObject go)
    {
        yield return new WaitForSeconds(2);
        Destroy(go);
        canMove = true;
        eating = false;
        princessNear = false;
        go.GetComponent<SphereCollider>().enabled = false;
    }
}

