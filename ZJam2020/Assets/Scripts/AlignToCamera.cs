﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignToCamera : MonoBehaviour
{
    private float yOffset;
    public float maxSpeed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Transform cameraTransform = Camera.main.transform;
        Vector3 cameraForwardRevised = cameraTransform.forward;
        cameraForwardRevised.y = 0;

        transform.LookAt(transform.position + cameraForwardRevised);
    }

    public void Bounce(Vector2 moveInputs)
    {
        if (moveInputs.x != 0 || moveInputs.y != 0)
        {
            yOffset = Mathf.Sin(Time.time * maxSpeed * 2) / 4;
            yOffset = Mathf.Abs(yOffset);
        }
        else
        {
            yOffset = 0;
        }
        //Debug.Log(yOffset); 
        //Transform spriteTransform = transform.GetChild(1).transform; 
        //spriteTransform.position = new Vector3(spriteTransform.position.x, yOffset, spriteTransform.position.z); 
        transform.localPosition = new Vector3(transform.localPosition.x, yOffset, transform.localPosition.z);
    }
}
