﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RotatingEnemy : MonoBehaviour
{
    public Material spritePointForward;
    public Material spritePointBack;
    MeshRenderer sprite;
    GameObject player;
    Vector3 origin;
    bool warning = false;
    bool restart = false;
    public int distance;
    NavMeshAgent navMeshAgent;
    AlignToCamera spriteController;
    int index = 0;
    public float time;
    public int rotation;

    private void Start()
    {
        sprite = transform.GetChild(0).GetComponent<MeshRenderer>();
        origin = transform.position;
        player = GameObject.Find("Player");
        navMeshAgent = GetComponent<NavMeshAgent>();

        spriteController = GetComponentInChildren<AlignToCamera>();
        Invoke("Rotate", time);
    }

    private void Update()
    {

        if (transform.forward.z > 0 && transform.forward.x == 0)
        {
            sprite.materials[0] = spritePointForward;
        }
        if (transform.forward.z < 0 && transform.forward.x == 0)
        {
            sprite.materials[0] = spritePointBack;
        }

        float difference = Vector3.Distance(transform.position, player.transform.position);
        Vector3 playerDirection = player.transform.position - transform.position;
        Debug.DrawRay(transform.position, playerDirection);

        if (warning)
        {
            navMeshAgent.SetDestination(player.transform.position);
        }
        if (difference < distance)
        {
            float angle = Vector3.Angle(playerDirection, transform.forward);
            if (angle < 15 && angle > -15)
            {
                if (Physics.Raycast(transform.position, playerDirection, out RaycastHit hit, playerDirection.magnitude))
                {
                    Debug.Log(hit.collider.gameObject.name);
                    if (hit.collider.gameObject.CompareTag("Player"))
                    {
                        warning = true;
                    }
                }
            }
        }
        else if (warning == true && difference > distance)
        {
            warning = false;
            navMeshAgent.SetDestination(origin);
            restart = true;
        }
        if(restart && transform.position == origin)
        {
            restart = false;
            Rotate();
        }
           
        // Si recibe un 0,0 para
        spriteController.Bounce(Vector2.one);
    }

    void Rotate()
    {
        if (warning == false)
        {
            transform.Rotate(Vector3.up * rotation);
        
            Invoke("Rotate", time);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Princess"))
        {
            navMeshAgent.SetDestination(other.transform.position);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player.GetComponent<PlayerController>();
        }
    }
}
