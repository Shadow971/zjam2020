﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour
{
    public Texture victorySprite;
    public GUISkin uISkin;

    public Transform point;
    public Transform nextCameraPoint;

    /*private void OnGUI()
    {
        if (victory)
        {
            GUI.DrawTexture(new Rect(Screen.width * 1 / 4, 0, Screen.width * 1 / 2, Screen.height), victorySprite);
            uISkin.customStyles[0].normal.textColor = Color.red;
            GUI.Label(new Rect(Screen.width * 1 / 4, Screen.height * 3/4, Screen.width * 1 / 2, 30), "Another night of beauty", uISkin.customStyles[0]);
            uISkin.customStyles[1].normal.textColor = Color.red;
            GUI.Label(new Rect(Screen.width * 1 / 4, Screen.height * 3 / 4 + 30, Screen.width * 1 / 2, 30), "Press ACTION to continue", uISkin.customStyles[1]);
        }
            
    }
    */

    public Transform Goto()
    {
        return point;
    }

    public Transform GotoCamera()
    {
        return nextCameraPoint;
    }
}
