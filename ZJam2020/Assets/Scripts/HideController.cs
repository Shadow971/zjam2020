using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideController : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;
    Light objLight;

    private float minDistance = 1;
    [SerializeField] float maxDistance = 8;

    Transform player;
    void Start()
    {
        player = FindObjectOfType<PlayerController>().transform;
        //objLight = transform.GetChild(0).GetComponent<Light>();
        objLight = GetComponentInChildren<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        //
        if(player == null) player = FindObjectOfType<PlayerController>().transform;
        if(objLight == null) objLight = GetComponentInChildren<Light>();
        //
        Vector3 playerDirection = player.position - transform.position;
        ray = new Ray(transform.position, playerDirection);
        float distance = Vector3.Distance(transform.position, player.position);
        if (distance < minDistance)
        {
            objLight.intensity = Mathf.Lerp(objLight.intensity, 1, Time.deltaTime * 10f);
        }
        else
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.CompareTag("Player"))
                {
                    if (distance > maxDistance)
                    {
                        float offset = (distance - maxDistance) / 2;
                        offset = Mathf.Clamp(offset, 0, 1);
                        objLight.intensity = Mathf.Lerp(objLight.intensity, 1 - offset, Time.deltaTime * 10f);
                    }
                    else
                    {
                        objLight.intensity = Mathf.Lerp(objLight.intensity, 1, Time.deltaTime * 10f);
                    }
                }
                else
                {
                    objLight.intensity = Mathf.Lerp(objLight.intensity, 0, Time.deltaTime * 10f);
                }
            }
        }

    }
}