﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    GameObject inTrigger;
    GameObject outTrigger;
    Transform leftDoor;
    Transform rightDoor;

    private bool playerNear = false;
    private bool enemyNear = false;

    void Start()
    {
        inTrigger = transform.GetChild(2).gameObject;
        outTrigger = transform.GetChild(3).gameObject;
        leftDoor = transform.GetChild(0);
        rightDoor = transform.GetChild(1);
    }

    public void Opened(int i)
    {
        transform.GetComponent<BoxCollider>().enabled = false;

        inTrigger.GetComponent<BoxCollider>().enabled = false;
        outTrigger.GetComponent<BoxCollider>().enabled = false;
        if (i == 0) //in
        {
            leftDoor.localEulerAngles += Vector3.up * 90;
            rightDoor.localEulerAngles -= Vector3.up * 90;
        }
        else
        {
            leftDoor.localEulerAngles -= Vector3.up * 90;
            rightDoor.localEulerAngles += Vector3.up * 90;
        }
        StartCoroutine(Close(3));

    }
    IEnumerator Close(float time)
    {
        yield return new WaitForSeconds(time);

        if(!playerNear && !enemyNear)
        {
            leftDoor.localEulerAngles = Vector3.zero;
            rightDoor.localEulerAngles = Vector3.zero;

            transform.GetComponent<BoxCollider>().enabled = true;
            inTrigger.GetComponent<BoxCollider>().enabled = true;
            outTrigger.GetComponent<BoxCollider>().enabled = true;
        }
        else
        {
            StartCoroutine(Close(1));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            playerNear = true;
        }

        if(other.GetComponent<EnemyIA>() != null)
        {
            enemyNear = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerNear = false;
        }
        if (other.GetComponent<EnemyIA>() != null)
        {
            enemyNear = false;
        }
    }
}