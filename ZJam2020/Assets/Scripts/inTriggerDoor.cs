﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inTriggerDoor : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player") || other.GetComponent<EnemyIA>() != null)
        {
            transform.parent.GetComponent<DoorController>().Opened(0);
        }
    }
}