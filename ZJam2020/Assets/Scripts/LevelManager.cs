﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject[] possibleRooms;
    public GameObject floorPrefab;

    private List<GameObject> allocatedRooms;
    // Start is called before the first frame update
    void Start()
    {
        //AllocateFloors();
        AllocateRooms();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void AllocateFloors()
    {
        //
        for(int i = 0; i < 7; i++)
        {
            //
            for(int j = 0; j < 30; j++)
            {
                Instantiate(floorPrefab, new Vector3((i - 3)*50, -0.5f, j * 50), Quaternion.identity);
            }
        }
    }

    void AllocateRooms()
    {
        //
        allocatedRooms = new List<GameObject>(20);
        //
        GameObject firstRoom = Instantiate(possibleRooms[0], Vector3.zero, Quaternion.identity);
        allocatedRooms.Add(firstRoom);
        //
        for(int i = 1; i < 19; i++)
        {
            int roomToSpawnIndex = Random.Range(0, possibleRooms.Length);
            GameObject nextRoom = Instantiate(possibleRooms[roomToSpawnIndex], Vector3.zero, possibleRooms[roomToSpawnIndex].transform.rotation);
            
            //
            Transform roomEntry = nextRoom.GetComponentInChildren<Entry>().transform;
            Transform previousRoomExit = allocatedRooms[allocatedRooms.Count - 1].GetComponentInChildren<ExitPoint>().transform;
            //
            nextRoom.transform.position = previousRoomExit.position - roomEntry.localPosition;
            //
            allocatedRooms.Add(nextRoom);
        }
    }
}
