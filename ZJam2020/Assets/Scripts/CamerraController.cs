﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerraController : MonoBehaviour
{
    public Vector3 maxOffset = new Vector3(0, 20, 20);
    public float lerpDuration = 1;

    private Transform pivot;
    private Vector3 originalPosition;
    private float offsetNormalized;

    private PlayerController playerController;

    private void Start()
    {
        pivot = transform.parent;
        playerController = FindObjectOfType<PlayerController>();

        originalPosition = transform.localPosition;
    }
    void Update()
    {
        transform.LookAt(pivot);
        //
        float dt = Time.deltaTime;
        //
        if (playerController.MoveInputs.magnitude != 0)
            offsetNormalized += dt;
        else
            offsetNormalized -= dt;
        //
        offsetNormalized = Mathf.Clamp(offsetNormalized, 0 , lerpDuration);
        //
        transform.localPosition = Vector3.Lerp(originalPosition, originalPosition + maxOffset, Mathf.Pow(offsetNormalized / lerpDuration, 2));
    }
}